import Vue from 'vue'
import store from '@/store'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
	{
		path: '/',
		name: 'Home',
		component: () => import("@/views/Home.vue"),
	},
	
	{
		path: '/category/:slug',
		name: 'category.view',
		component: () => import("@/views/CategoryView.vue"),
	},
	{
		path: '/product/:slug',
		name: 'product.view',
		component: () => import("@/views/ProductView.vue"),
		meta: { 
			reload: true 
		}
	},
	{
		path: '/checkout',
		name: 'checkout',
		component: () => import("@/views/Checkout.vue"),
		meta: { 
			auth: true 
		}
	},
	{
		path: '/order-summary/:orderNumber',
		name: 'Ordersummary',
		component: () => import("@/views/Ordersummary.vue"),
		meta: { 
			auth: true 
		}
	},
	{
		path: '/profile',
		name: 'Userprofile',
		component: () => import("@/views/Userprofile.vue"),
		meta: { 
			auth: true 
		}
	},
	{
		path: '/:slug',
		name: 'ProductList',
		component: () => import("@/views/ProductList.vue"),
	}
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
	const requiresAuth = to.matched.some(record => record.meta.auth);
	if (requiresAuth && !store.getters.getLoggedUser) {
		sessionStorage.setItem('redirectPath', to.path);
		store.dispatch('openLogInDialog',true)
	} else if (requiresAuth && store.getters.getLoggedUser) {
		next();
	} else {
		next();
	}
  
	/* if(to.matched.some(record => record.meta.reload)) {
		window.location.reload()
	} */
})

export default router
