export const imageURL = 'https://admin.freshoppe.com/uploads/';

export const orderStatus = {
				'1': 'Placed',
				'2': 'Processing',
				'3': 'Dispatched',
				'4': 'On Delivery',
				'5': 'Completed',
				'6': 'Declined',
				'7': 'Cancelled'
};

export const deliveryTimeSlots = {
				'1': '9AM - 11AM',
				'2': '11AM - 1PM',
				'3': '1PM - 3PM',
				'4': '3PM - 5PM',
				'5': '5PM - 7PM'
};
            