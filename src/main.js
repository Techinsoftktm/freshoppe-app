import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import router from './router'
import store from './store'
import VueGeolocation from 'vue-browser-geolocation';
import { BootstrapVue } from 'bootstrap-vue'
import Notifications from 'vue-notification'
import ProductZoomer from 'vue-product-zoomer'
import GoogleAuth from '@/config/google.js'
import VueMeta from 'vue-meta'
import VueSocialSharing from 'vue-social-sharing'
import VueProgressBar from 'vue-progressbar'
import VueMoment from 'vue-moment'
import moment from 'moment-timezone'

const gauthOption = {
  clientId: '638256668099-00m7nueo00s4s0s41trgo75c0bccolcj.apps.googleusercontent.com',
  scope: 'profile email',
  prompt: 'select_account'
}
//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
Vue.config.productionTip = false
Vue.use(GoogleAuth, gauthOption)
Vue.use(BootstrapVue)
//Vue.use(IconsPlugin)
Vue.use(VueGeolocation);
Vue.use(Notifications)
Vue.use(ProductZoomer)
Vue.use(VueMeta)
//Vue.use(require('vue-moment'));
Vue.use(VueSocialSharing);

//const moment = require('moment')
//require('moment/locale/en')
 
Vue.use(require('vue-moment'), {
    moment
})

const options = {
  color: '#1b1464',
  transition: {
    speed: '0.2s',
    opacity: '0.6s',
    termination: 300,
	inverse: false,
	autoRevert: true,
  },
}

Vue.use(VueProgressBar, options)


var filter = function(text, length, clamp){
    clamp = clamp || '...';
    var node = document.createElement('div');
    node.innerHTML = text;
    var content = node.textContent;
    return content.length > length ? content.slice(0, length) + clamp : content;
};

Vue.filter('truncate', filter);


import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './assets/app.scss'
import './assets/header.scss'
import './assets/home.scss'
import './assets/fonts.scss'

window.axios = axios

axios.defaults.baseURL = 'https://api.freshoppe.com'

// Token


// Request Interceptor
axios.interceptors.request.use(config => {
    //store.dispatch('displayLoader', true)
	
	if (store.getters.getLoggedUser) {
		config.headers['Authorization'] = 'Bearer ' + store.getters.getLoggedUser.token
	}
    return config
}, error => {
    //store.dispatch('displayLoader', false)

    return Promise.reject(error)
})


// Response Interceptor
axios.interceptors.response.use(response => {
    //store.dispatch('displayLoader', false)
    return response
}, error => {
    //store.dispatch('displayLoader', false)
    var errors = error
    if (error.response) {
        // Session Expired
        if (401 === error.response.status) {
			console.log(error)
            errors = error.response.data.message
            //store.dispatch('logOut')
        }

        // Errors from backend
        if (error.response.status == 422) {
            errors = '';
            for(var errorKey in error.response.data) {
                for(var i = 0; i < error.response.data[errorKey].length; i++) {
                    errors += (String(error.response.data[errorKey][i])) + '<br>'
                }
            }
        }

        // Backend error
        if (500 === error.response.status) {
            errors = error.response.data.message
        }

        // 404
        if (error.response.status == 404) {
            errors = 'Page not found'
        }
		
		if (409 === error.response.status) {
            errors = error.response.data.message
        }
    }
    Vue.notify({
        group: 'notify',
        type: 'error',
        title: 'Error',
        text: String(errors),
    })

    return Promise.reject(error)
})

export const bus = new Vue();
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
