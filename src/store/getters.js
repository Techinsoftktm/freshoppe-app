const getDisplayLoader = (state) => {
	return state.displayLoader
}

const getLoggedUser = (state) => {
    let user = state.loggedUser
    if(user) {
        user = JSON.parse(user)
    }
	return user
}

const cartProducts = (state) => {
    return state.cartItems
}

const cartTotalPrice = (state) => {
    return state.cartItems.reduce((total, product) => {
      return total + product.price * product.quantity
    }, 0)
}

const cartTotalGST = (state) => {
    return state.cartItems.reduce((gst, product) => {
	  return gst + (((product.price * product.quantity) * product.gst_rate)/100);
    }, 0)
}
 
const customerAddresses = (state) => {
    return state.customerAddress
}

const getCurrentAddress = (state) => {
    return state.firstAddress
}

const getFavourites = (state) => {
    return state.customerFavourites
}

export default {
    getDisplayLoader,
    getLoggedUser,
	cartProducts,
	cartTotalPrice,
	customerAddresses,
	getCurrentAddress,
	getFavourites,
	cartTotalGST
}