const DISPLAY_LOADER = (state, display) => {
	state.displayLoader = display
}

const LOGGED_USER = (state, user) => {
    // let now = new Date()
    // let expiryDate = new Date()
    // user.expiryDate = expiryDate.setTime(now.getTime() + user.expires_in * 1000)

    localStorage.setItem('loggedUser', JSON.stringify(user))
    state.loggedUser = JSON.stringify(user)
	axios.get('/users/profile')
	.then(response => {
		state.customerAddress 		= response.data.data.address;
		state.customerFavourites 	= response.data.data.favourites;
		if(state.customerAddress.length > 0 && state.firstAddress.length == 0){
			state.firstAddress = state.customerAddress[0]
		}
	})
	.catch(function (error) {
		console.log(error);
	});
}

const REMOVE_LOGGED_USER = (state) => {
    state.cartItems = []
    state.customerAddress = []
    state.firstAddress = []
    state.customerFavourites = []
	if(state.loggedUser != null){
		axios.post('/clear-cart')
		.then(response => {
		})
		.catch(function (error) {
			console.log(error);
		});
	}
	state.loggedUser = null
	localStorage.removeItem('loggedUser') 
}

const OPEN_LOGIN = (state,display) => {
	state.loginModal = display;
}

const STORE_CART = (state) => {
	if(state.loggedUser != null ){
		axios.post('/store-cart-data',{
			"cartItems": state.cartItems
		})
		.then(response => {
			state.cartItems = response.data.data.cart;
		})
		.catch(function (error) {
			console.log(error);
		});
		
		if(state.customerAddress.length > 0 && state.firstAddress.length == 0){
			state.firstAddress = state.customerAddress[0]
		}
		
	}
	
}

const PUSH_PRODUCT_TO_CART = (state, { productInfo }) => {
	const cartItem = state.cartItems.find(cartProduct => cartProduct.id == productInfo.id)
	if (!cartItem) {
		var originalPrice = 0;
		if(productInfo.sale_price > 0)
			originalPrice = productInfo.sale_price;
		else
			originalPrice = productInfo.price;
		
		state.cartItems.push({
		  id:productInfo.id,
		  quantity: 1,
		  name: productInfo.ecom_name,
		  price: originalPrice,
		  netWt: productInfo.net_weight,
		  grossWt: productInfo.gross_weight,
		  unit: productInfo.unit_id,
		  pieces: productInfo.pieces,
		  gst_rate: productInfo.gst_rate,
		})
		if(state.loggedUser != null){
			axios.post('/add-to-cart',{
				"productId": productInfo.id
			})
			.then(response => {
				state.cartItems = response.data.data.cart;
			})
			.catch(function (error) {
				console.log(error);
			});
		}
		
	} else {
		cartItem.quantity++
		if(state.loggedUser != null){
			axios.post('/increment-item-qty',{
				"productId": productInfo.id
			})
			.then(response => {
				state.cartItems = response.data.data.cart;
			})
			.catch(function (error) {
				console.log(error);
			});
		}
		
	}
}

const SET_CART_ITEMS = (state, { items }) => {
    state.cartItems = items
}


const SET_CHECKOUT_STATUS = (state, status) => {
    state.checkoutStatus = status
}

const REMOVE_PRODUCT_FROM_CART = (state, id) => {
	const i = state.cartItems.map(item => item.id).indexOf(id);
    state.cartItems.splice(i, 1);
	if(state.loggedUser != null){
		axios.post('/remove-from-cart',{
			"productId": id
		})
		.then(response => {
			state.cartItems = response.data.data.cart;
		})
		.catch(function (error) {
			console.log(error);
		});
	}
}

const INCREMENT_ITEM_QTY = (state, id) => {
	const cartItem = state.cartItems.find(cartProduct => cartProduct.id == id)
	cartItem.quantity++
	if(state.loggedUser != null){
		axios.post('/increment-item-qty',{
			"productId": id
		})
		.then(response => {
			state.cartItems = response.data.data.cart;
		})
		.catch(function (error) {
			console.log(error);
		});
	}
}

const DECREMENT_ITEM_QTY = (state, id) => {
	const cartItem = state.cartItems.find(cartProduct => cartProduct.id == id)
	if(cartItem.quantity == 1){
		const i = state.cartItems.map(item => item.id).indexOf(id);
		state.cartItems.splice(i, 1);
		if(state.loggedUser != null){
			axios.post('/remove-from-cart',{
				"productId": id
			})
			.then(response => {
				state.cartItems = response.data.data.cart;
			})
			.catch(function (error) {
				console.log(error);
			});
		}
	}else{
		cartItem.quantity--
		if(state.loggedUser != null){
			axios.post('/decrement-item-qty',{
				"productId": id
			})
			.then(response => {
				state.cartItems = response.data.data.cart;
			})
			.catch(function (error) {
				console.log(error);
			});
		}
	}
	
	
}

const UPDATE_QTY = (state, {productId, qty}) => {
	const cartItem = state.cartItems.find(cartProduct => cartProduct.id == productId)
	cartItem.quantity = qty;
}

const ADD_ADDRESS = (state, { addressInfo }) => {
	state.customerAddress.push({
	  id:addressInfo.id,
	  name: addressInfo.name,
	  mobile: addressInfo.mobile,
	  address_type: addressInfo.address_type,
	  area: addressInfo.area,
	  flat: addressInfo.flat,
	  landmark: addressInfo.landmark,
	  user_id: addressInfo.user_id,
	  address: addressInfo.address
	})
}

const UPDATE_ADDRESS = (state, { addressInfo }) => {
	const addressItem = state.customerAddress.find(address => address.id == addressInfo.id)
	if (addressItem) {
		addressItem.id 			= addressInfo.id,
		addressItem.name 		= addressInfo.name,
		addressItem.mobile		= addressInfo.mobile,
		addressItem.address_type= addressInfo.address_type,
		addressItem.area		= addressInfo.area,
		addressItem.flat		= addressInfo.flat,
		addressItem.landmark	= addressInfo.landmark,
		addressItem.user_id		= addressInfo.user_id,
		addressItem.address		= addressInfo.address
	}
}

const SET_CURRENT_ADDRESS = (state, { addressInfo }) => {
	state.firstAddress = addressInfo;
}

const ADD_FAVOURITE = (state, { favInfo }) => {
	state.customerFavourites = favInfo;
}

const REMOVE_FAVOURITE = (state, id) => {
	const i = state.customerFavourites.map(item => item.id).indexOf(id);
    state.customerFavourites.splice(i, 1);
	if(state.loggedUser != null){
		axios.post('/product/favourite', {productId : id})
		.then(response => {
			state.customerFavourites = response.data.data.favourites;
		})
		.catch(function (error) {
			console.log(error);
		});
	}
}

const SET_ADDRESS = (state, { addressInfo }) => {
    state.customerAddress = addressInfo
}

const CLEAR_CART_ITEMS = (state) => {
    state.cartItems = []
}

export default {
    DISPLAY_LOADER,
    LOGGED_USER,
    REMOVE_LOGGED_USER,
	OPEN_LOGIN,
	PUSH_PRODUCT_TO_CART,
	SET_CHECKOUT_STATUS,
	REMOVE_PRODUCT_FROM_CART,
	INCREMENT_ITEM_QTY,
	DECREMENT_ITEM_QTY,
	UPDATE_QTY,
	STORE_CART,
	ADD_ADDRESS,
	SET_CURRENT_ADDRESS,
	UPDATE_ADDRESS,
	ADD_FAVOURITE,
	REMOVE_FAVOURITE,
	SET_ADDRESS,
	SET_CART_ITEMS,
	CLEAR_CART_ITEMS
}

