import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex)

import actions from './actions'
import getters from './getters'
import mutations from './mutations'

const state = {
	displayLoader: false,
	loginModal: false,
	loggedUser: localStorage.getItem('loggedUser') || null,
	cartItems: [],
	customerAddress: [],
	checkoutStatus: null,
	customerFavourites: [],
	firstAddress:[]
}

export default new Vuex.Store({
	state,
	actions,
	getters,
	mutations,
	plugins: [createPersistedState()]
})
