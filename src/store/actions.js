import Vue from 'vue'
import Notifications from 'vue-notification'

const displayLoader = (context, display) => {
	context.commit('DISPLAY_LOADER', display)
}

const loggedUser = (context, user) => {
	context.commit('LOGGED_USER', user)
}

const logOut = context => {
	context.commit('REMOVE_LOGGED_USER')
}

const openLogInDialog = (context, display) => {
	context.commit('OPEN_LOGIN' ,display)
}

const addProductToCart = (context, product) => {
	context.commit('SET_CHECKOUT_STATUS', null)
	context.commit('PUSH_PRODUCT_TO_CART', { productInfo: product })
	/* Vue.notify({
        group: 'notify',
        type: 'success',
        title: 'Success',
        text: 'Product added to cart',
    }) */
}
 
const removeProductFromCart = (context, productId) => {
	context.commit('REMOVE_PRODUCT_FROM_CART',productId)
}

const incrementQuantity = (context, productId) => {
	context.commit('INCREMENT_ITEM_QTY',productId)
}

const decrementQuantity = (context, productId) => {
	context.commit('DECREMENT_ITEM_QTY',productId)
}

const updateQuantity = (context, {productId, qty}) => {
	context.commit('UPDATE_QTY',{productId, qty})
}

const storeCart = context => {
	context.commit('STORE_CART')
}

const setCartItems = (context, items) => {
	context.commit('SET_CART_ITEMS',{ items: items })
}

const addAddress = (context, address) => {
	context.commit('ADD_ADDRESS', { addressInfo: address })
}

const chooseAddress = (context, address) => {
	context.commit('SET_CURRENT_ADDRESS', { addressInfo: address })
}

const updateAddress = (context, address) => {
	context.commit('UPDATE_ADDRESS', { addressInfo: address })
}

const setAddress = (context, address) => {
	context.commit('SET_ADDRESS', { addressInfo: address })
}

const setFavourite = (context, favourite) => {
	context.commit('ADD_FAVOURITE', { favInfo: favourite })
}

const removeFavourite = (context, productId) => {
	context.commit('REMOVE_FAVOURITE',productId)
}

const clearCart = (context) => {
	context.commit('CLEAR_CART_ITEMS')
}

export default {
	displayLoader,
	loggedUser,
	logOut,
	openLogInDialog,
	addProductToCart,
	removeProductFromCart,
	incrementQuantity,
	decrementQuantity,
	updateQuantity,
	storeCart,
	addAddress,
	chooseAddress,
	updateAddress,
	setFavourite,
	removeFavourite,
	setAddress,
	setCartItems,
	clearCart
}